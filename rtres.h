//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by T55RT.rc
//
#define IDR_MENU                        101
#define ID_FILE_ABOUT                   40001
#define ID_HELP_ABOUT                   40002
#define ID_FILE_EXIT                    40003
#define ID_FILE_SAVE                    40004
#define ID_FILE_START                   40005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40006
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
