Scene.AddMtl("Sphere1", illum(color(0.05), color(0.6), color(0.9), 30));
  Scene.AddMtl("Sphere2", illum(color(0.05), color(0.3, 0.7, 0.5), color(0.4, 0.9, 0.8), 10));
  Scene.AddMtl("Sphere3", illum(color(0.05), color(0.9, 0.3, 0.4), color(0.3, 0.7, 0.4), 8)); 
  Scene.AddMtl("Sphere4", illum(color(0.05), color(0.9, 0.7, 0.8), color(0.2, 0.5, 0.7), 20));
  Scene.AddMtl("Plane", illum(color(0), color(1), color(0.5), 25));

  Scene << new shape_plane(vec(0, 1, 0), 0, color(0.6, 0.7, 0.2), Scene.GetMtl("Plane"));
  for (INT i = 0; i < 15; i++)
  {
    DBL R = (DBL)rand() / RAND_MAX * 2.5 + 0.5;
    std::string S;
    switch (rand() % 4)
    {
    case 0:
      S = std::string("Sphere1");
      break;
    case 1:
      S = std::string("Sphere2");
      break;
    case 2:
      S = std::string("Sphere3");
      break;
    case 3:
      S = std::string("Sphere4");
      break;
    }
    Scene << new shape_sphere(vec(((DBL)rand() / RAND_MAX - 0.5) * 70, R, ((DBL)rand() / RAND_MAX - 0.5) * 70), R, color::R0(), Scene.GetMtl(S));
  }
  for (INT i = 0; i < 15; i++)
  {
    Scene << new point_light(vec(((DBL)rand() / RAND_MAX - 0.5) * 70, 10 + (DBL)rand() / RAND_MAX * 20, ((DBL)rand() / RAND_MAX - 0.5) * 70), color::R0());
  }